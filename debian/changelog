edenmath.app (1.1.1a-9) UNRELEASED; urgency=medium

  * Use secure URI in Homepage field.
  * Bump debhelper from old 11 to 12.

 -- Debian Janitor <janitor@jelmer.uk>  Mon, 09 Dec 2019 06:34:59 +0000

edenmath.app (1.1.1a-8) unstable; urgency=medium

  * debian/compat: Bump to 11.
  * debian/source/format: New file, set to 3.0 (quilt).
  * GNUmakefile: Move local modifications...
  * debian/patches/include-help.patch: ...here.
  * debian/patches/series: New file.
  * debian/rules: Rewrite for modern dh.  Move Resources to /usr/share.
    Enable hardening.  Do not use gs_make (Closes: #897622).
    (override_dh_compress): Exclude .rtf files.
  * debian/maintscript: New file, handle the dir to symlink switch.
  * debian/control: Run wrap-and-sort -ast for diff-friendliness.
    (Uploaders): Remove Hubert on his request; add myself.
    (Build-Depends): Bump debhelper to >= 11.  Drop ancient
    libgnustep-gui-dev version constraint.  Require gnustep-make >=
    2.7.0-3 for the optim variable definition.
    (Vcs-Git, Vcs-Browser): New fields.
    (Standards-Version): Compliant with 4.1.4 as of this release.
    (Description): Mention the original app.
  * debian/changelog: Whitespace cleanup.
  * debian/lintian-override:
  * debian/menu:
  * debian/dirs: Delete; no longer necessary.
  * debian/install: New file; install the .desktop file.
  * debian/EdenMath.desktop: Remove invalid Version key, add Keywords, set
    Icon to the real file in /usr/share.
  * debian/watch: New file.
  * debian/copyright: Rewrite in format 1.0.

 -- Yavor Doganov <yavor@gnu.org>  Sat, 05 May 2018 19:31:22 +0300

edenmath.app (1.1.1a-7.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Bump debhelper version. Closes: #817445.
  * Fix some lintian warnings.

 -- Matthias Klose <doko@ubuntu.com>  Fri, 02 Sep 2016 09:03:14 +0200

edenmath.app (1.1.1a-7build7) yakkety; urgency=medium

  * No-change rebuild for gnustep-gui 0.25

 -- Matthias Klose <doko@ubuntu.com>  Thu, 01 Sep 2016 21:11:20 +0000

edenmath.app (1.1.1a-7build6) utopic; urgency=medium

  * Rebuild against libgnustep-gui0.24.

 -- Colin Watson <cjwatson@ubuntu.com>  Thu, 28 Aug 2014 11:12:34 -0700

edenmath.app (1.1.1a-7build5) raring; urgency=low

  * Rebuild for gnustep-gui 0.22 transition.

 -- Benjamin Drung <bdrung@ubuntu.com>  Wed, 21 Nov 2012 16:48:01 +0100

edenmath.app (1.1.1a-7build4) oneiric; urgency=low

  * Rebuild for gnustep-base 0.22 and gnustep-gui 0.20.

 -- Colin Watson <cjwatson@ubuntu.com>  Mon, 11 Jul 2011 11:22:32 +0100

edenmath.app (1.1.1a-7build3) natty; urgency=low

  * Rebuild for the GNUstep transition.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Fri, 29 Oct 2010 20:45:07 +0200

edenmath.app (1.1.1a-7build2) karmic; urgency=low

  * Rebuild for the GNUstep transition.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Sun, 24 May 2009 13:30:16 +0000

edenmath.app (1.1.1a-7build1) intrepid; urgency=low

  * Rebuild for GNUstep transition.

 -- Luca Falavigna <dktrkranz@ubuntu.com>  Sun, 06 Jul 2008 14:53:17 +0200

edenmath.app (1.1.1a-7) unstable; urgency=low

  * Adopted by the GNUstep maintainers. (Closes: #454442)
  * Fix typo in package description. (Closes: #449476)

 -- Hubert Chathi <uhoreg@debian.org>  Fri, 09 May 2008 18:16:57 -0400

edenmath.app (1.1.1a-6) unstable; urgency=medium

  * QA upload.
  * debian/rules:
    + Separate CFLAGS into OPTFLAGS and debug while passing
      arguments to gs_make in order to prevent an FTBFS.
      (Closes: #476060)
  * debian/control:
    + Standards Version is now 3.7.3 (no changes needed).
    + Update to use new Homepage field.

 -- Kumar Appaiah <akumar@debian.org>  Fri, 18 Apr 2008 20:52:59 +0530

edenmath.app (1.1.1a-5) unstable; urgency=low

  * Orphaning package, setting maintainer to the Debian QA Group.

 -- Ana Beatriz Guerrero Lopez <ana@debian.org>  Fri, 07 Dec 2007 02:22:13 +0100

edenmath.app (1.1.1a-4) unstable; urgency=low

  * GNUstep transition.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sat, 06 Oct 2007 16:08:11 +0200

edenmath.app (1.1.1a-3) unstable; urgency=low

  * Rebuild against latest libgnustep-gui.
  * Bump standards version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 24 Sep 2006 14:36:20 +0200

edenmath.app (1.1.1a-2) unstable; urgency=low

  * Rebuild against latest libgnustep-gui.
  * Update manual page.
  * Bump standards version.

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Mon, 23 Jan 2006 21:30:31 +0100

edenmath.app (1.1.1a-1) unstable; urgency=low

  * Updated debian/control build-depends for GNUstep 0.9.4.
  * Correct *.orig.tar.gz

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Sun, 21 Nov 2004 18:24:10 +0100

edenmath.app (1.1.1-1) unstable; urgency=low

  * Initial Release.
  * Fixed VERSION and added .help to RESOURCES in GNUmakefile

 -- Gürkan Sengün <gurkan@linuks.mine.nu>  Wed, 15 Sep 2004 17:09:15 +0200
